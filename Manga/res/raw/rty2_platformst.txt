|2|Linux|linux.png|4
|3|Mac OS|Mac.png|4
|4|Windows|Windows.png|4
|5|Firefox|firefox.png|5
|6|Access|Office-Access-icon.png|5
|7|Office 2007|Office-icon.png|5
|9|OneNote 2007|OneNote-icon.png|5
|10|Oracle SQL Developer|sqldevelopericons.png|5
|11|Outlook|Outlook-icon.png|5
|12|Photoshop|photoshop-icon.png|5
|13|Adobe Illustrator|Adobe_Illustrator_icon.png|5
|14|Adobe Reader|Adobe-Acrobat-Reader-icon.png|5
|15|After Effects|Adobe-After-Effects-CS-3-icon.png|5
|16|Android Emulator|Android-Emulator-Icon.png|5
|17|Avid Media Composer|Avid-Icons.jpg|5
|18|Google Chrome|Google-Chrome-icon.png|5
|19|Civilization V|Civilization-IV-Colonization-icon.png|6
|20|Clementine|Apps-clementine-icon.png|5
|21|Eclipse|Apps-eclipse-icon.png|5
|22|Photoshop Lightroom|Adobe-Lightroom-CS-4-icon.png|5
|23|PowerPoint|PowerPoint-icon.png|5
|24|Evernote|evernote-icon.png|5
|25|Excel|Excel-icon.png|5
|26|Firebug|firebug-programme-icon.png|5
|27|Fireworks|Fireworks-4-icon.png|5
|28|FreeMind|freemind-icon.jpg|5
|29|Google Docs|Web-Google-Docs-Metro-icon.png|5
|30|Google Earth|Google-Earth-icon.png|5
|31|InDesign|InDesign-4-icon.png|5
|32|Internet Explorer|Internet-Explorer-icon.png|5
|33|Media Player Classic|Media-Player-Classic-icon.png|5
|34|Microsoft Dynamics CRM|Microsoft-Dynamics-CRM-icon.png|5
|35|Notepad plus plus|Notepad_plus_plus.png|5
|36|Word|Word-icon.png|5
|37|Windows Media Player|Windows-Media-Player-icon.png|5
|38|Winamp|Winamp-icon.png|5
|39|VLC Media Player|Media-vlc-icon.png|5
|40|Visual Studio|Apps-Visual-Studio-alt-Metro-icon.png|5
|41|Total Commander|Total-Commander-icon.png|5
|42|Thunderbird|Thunderbird-icon.png|5
|43|Tekla Structures|Tekla-structures-icon.jpg|5
|44|Sublime Text|sublime-text-icon.png|5
|45|Sticky Notes|Sticky-Notes-icon.png|5
|46|Skype|Skype-icon.png|5
|47|SharpDevelop|SharpDevelop-4.3-icon.png|5
|48|Quickbooks|Quickbooks-icon.png|5
|49|Projects|Project-icon.png|5
|50|Photo Viewer|Apps-Picture-Viewer-Metro-icon.png|5
|51|Zune|Apps-Zune-alt-Metro-icon.png|5
|52|Zoho Sheet|icon-zohosheet-lg.png|5
|53|WinSCP|WinSCP-icon.gif|5