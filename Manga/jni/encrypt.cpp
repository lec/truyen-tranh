

#include "Rijndael.h"
#include "base64.h"
#include <android/log.h>
using namespace std;
char key[]="fd5d244026ed99ae";
extern "C" const char * EncryptAES(char *szDataIn)
{
	int len=16*(strlen(szDataIn)/16);
	CRijndael oRijndael;
	oRijndael.MakeKey(key, CRijndael::sm_chain0, 16, 16);
	char szDataOut[256];
	oRijndael.Encrypt(szDataIn, szDataOut,len);
//	__android_log_print(ANDROID_LOG_INFO, "tdtanvn", "%s %d",szDataIn,len);
	std::string encoded = base64_encode(reinterpret_cast<const unsigned char*>(szDataOut), len);
	return encoded.c_str();
}
extern "C" const char * DecryptAES(const char * szDataIn){

	const std::string s = szDataIn;
	std::string encoded = base64_decode(s);
	CRijndael oRijndael;
	oRijndael.MakeKey(key, CRijndael::sm_chain0, 16, 16);
	char szDataOut[256]={'\0'};
	const char *temp=encoded.c_str();
	oRijndael.Decrypt(temp, szDataOut,encoded.size());
	return szDataOut;
}
