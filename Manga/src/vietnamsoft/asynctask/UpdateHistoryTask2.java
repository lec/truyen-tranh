package vietnamsoft.asynctask;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import vietnamsoft.manga.adapter.PackageItem;
import vietnamsoft.manga.utl.MockUtils;
import android.os.AsyncTask;
import android.os.Environment;

public class UpdateHistoryTask2 extends AsyncTask<PackageItem, Void, List<PackageItem>>{
	@SuppressWarnings("unchecked")
	@Override
	protected List<PackageItem> doInBackground(PackageItem... params) {
		List<PackageItem> rs = null;
		File extStore = Environment.getExternalStorageDirectory();
		PackageItem ob = params[0];
		try {
			rs = (ArrayList<PackageItem>)MockUtils.readTestdatafromFileSystem(extStore.getAbsolutePath() + "/manga24h/history.dat");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (rs == null)
		{
			rs = new ArrayList<PackageItem>();
		}
		if (!rs.contains(ob))
		{
			rs.add(ob);
		}
		return rs;
	}

	@Override
	protected void onPostExecute(List<PackageItem> result) {
		if(result != null && result.size() > 0)
		{
			File extStore = Environment.getExternalStorageDirectory();
			try {
				MockUtils.writeTestdataToFileSystem(extStore.getAbsolutePath() + "/manga24h/history.dat", result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onPreExecute() {
	}

	@Override
	protected void onProgressUpdate(Void... values) {
	}
}
