package vietnamsoft.asynctask;

import java.util.List;


import vietnamsoft.manga.utl.IMangaConst;
import vietnamsoft.manga.utl.MockUtils;
import vn.vietnamsoft.manga.impl.DataProcessImpl;
import vn.vietnamsoft.manga.inteface.IDataProcess;
import vn.vietnamsoft.manga.vo.Manga;
import android.os.AsyncTask;

public class UpdateMangaTask extends AsyncTask<String, Void, List<Manga>>{
	private String rootPath;
	public void setRootPath(String path)
	{
		rootPath = path;
	}
	@Override
	protected List<Manga> doInBackground(String... params) {
		IDataProcess dt = new DataProcessImpl();
		//get list hot
		try {
			MockUtils.writeTestdataToFileSystem(rootPath + IMangaConst.MangaHotDataPath, dt.getListManga(IMangaConst.MANGA, IMangaConst.MANGA_HOT_QUERY));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dt.getListManga(params[0], params[1]);
	}

	@Override
	protected void onPostExecute(List<Manga> result) {
		if(result != null && result.size() > 0)
		{
			try {
				MockUtils.writeTestdataToFileSystem(rootPath + IMangaConst.MangaDataPath, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
