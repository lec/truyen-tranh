package vietnamsoft.asynctask;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import vietnamsoft.manga.IndexableListViewActivity;
import vn.vietnamsoft.manga.vo.Page;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class DownloadTask extends AsyncTask<List<Page>, Void, Boolean>{
	private String chapterFolder;
	private Activity ctx1;
	public void setChapterFolder(String chapter)
	{
		chapterFolder = chapter;
	}
	public void setContext(Activity ctxa)
	{
		ctx1 = ctxa;
	}
	@Override
	protected Boolean doInBackground(List<Page> ...lst) {
		if (lst != null && lst.length > 0 )
		{
			int count = 1;
			int length = lst[0].size();
			for (Page item : lst[0])
			{
				String filename = chapterFolder + "/" + count + ".jpg";
				Bitmap bmp = downloadBitmap(item.getUrlImage());
				try {
		            FileOutputStream stream = new FileOutputStream(filename);
			    	ByteArrayOutputStream outstream = new ByteArrayOutputStream();
			    	bmp.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
		            byte[] byteArray = outstream.toByteArray();
		            stream.write(byteArray);
		            stream.close();
		            if (!ctx1.isFinishing())
		    		{
		            	Crouton.makeText(ctx1, "Đã tải xuống " + count + "/" + length, Style.INFO).show();
		    		}
		            count ++;
			    }
			    catch(Exception e)
			    {
			    	Log.w("err", e.getMessage());
			    }
			}
		}
		return true;
	}
	private Bitmap downloadBitmap(String url) {
        // initilize the default HTTP client object
        final DefaultHttpClient client = new DefaultHttpClient();

        //forming a HttoGet request
        final HttpGet getRequest = new HttpGet(url);
        try {

            HttpResponse response = client.execute(getRequest);

            //check 200 OK for success
            final int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode != HttpStatus.SC_OK) {
                Log.w("ImageDownloader", "Error " + statusCode +
                        " while retrieving bitmap from " + url);
                return null;

            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    // getting contents from the stream
                    inputStream = entity.getContent();

                    // decoding stream data back into image Bitmap that android understands
                    final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                    return bitmap;
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (Exception e) {
            // You Could provide a more explicit error message for IOException
            getRequest.abort();
            Log.e("ImageDownloader", "Something went wrong while" +
                    " retrieving bitmap from " + url + e.toString());
        }

        return null;
    }
	@Override
	protected void onPostExecute(Boolean result) {
		if (!ctx1.isFinishing())
		{
			Crouton.makeText(ctx1, "Đã tải xuống máy thành công", Style.INFO).show();
		}
		IndexableListViewActivity.isDownloading = false;
	}

	@Override
	protected void onPreExecute() {
	}

	@Override
	protected void onProgressUpdate(Void... values) {
	}
}
