package vietnamsoft.asynctask;

import java.util.List;

import vn.vietnamsoft.manga.impl.DataProcessImpl;
import vn.vietnamsoft.manga.inteface.IDataProcess;
import vn.vietnamsoft.manga.vo.Manga;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class MangaTask extends AsyncTask<String, Void, List<Manga>>{
	ProgressDialog progress; 
	Context ctx;
	String messages = "";
	public void setProgress(String message, Context pctx)
	{
		messages = message;
		ctx = pctx;
	}
	@Override
	protected List<Manga> doInBackground(String... params) {
		IDataProcess dt = new DataProcessImpl();
		return dt.getListManga(params[0], params[1]);
	}

	@Override
	protected void onPostExecute(List<Manga> result) {
		if (progress != null)
		{
			progress.dismiss();
		}
	}

	@Override
	protected void onPreExecute() {
		progress = ProgressDialog.show(ctx, "loading",
        	    messages, true);
	}

	@Override
	protected void onProgressUpdate(Void... values) {
	}
}
