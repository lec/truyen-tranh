package vietnamsoft.asynctask;

import java.util.List;

import vn.vietnamsoft.manga.impl.DataProcessImpl;
import vn.vietnamsoft.manga.inteface.IDataProcess;
import vn.vietnamsoft.manga.vo.Page;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class PageTask extends AsyncTask<String, Void, List<Page>>{
	ProgressDialog progress; 
	Context ctx;
	String messages = "";
	public void setProgress(String message, Context pctx)
	{
		messages = message;
		ctx = pctx;
	}
	@Override
	protected List<Page> doInBackground(String... params) {
		IDataProcess dt = new DataProcessImpl();
		return dt.getListPage(params[0], params[1]);
	}

	@Override
	protected void onPostExecute(List<Page> result) {
		if (progress != null)
		{
			progress.dismiss();
		}
	}

	@Override
	protected void onPreExecute() {
		progress = ProgressDialog.show(ctx, "loading",
        	    messages, true);
	}

	@Override
	protected void onProgressUpdate(Void... values) {
	}
}
