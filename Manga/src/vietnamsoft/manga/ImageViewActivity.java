
package vietnamsoft.manga;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import vietnamsoft.manga.ViewPagerActivity.SamplePagerAdapter;
import vietnamsoft.manga.utl.IMangaConst;
import vietnamsoft.photo.HackyViewPager;
import vn.vietnamsoft.manga.impl.DataProcessImplV2;
import vn.vietnamsoft.manga.inteface.IDataProcess;
import vn.vietnamsoft.manga.vo.Page;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
public class ImageViewActivity extends Activity{
	private static final String STATE_POSITION = "STATE_POSITION";
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	ViewPager pager;
	DisplayImageOptions options;
	class PageOnlineTask extends AsyncTask<String, Void, List<Page>>{
    	@Override
    	protected List<Page> doInBackground(String... params) {
    		/*long time = System.currentTimeMillis();
    		long ex = 0;*/
    		List<Page> res = new ArrayList<Page>();
    		IDataProcess dt = new DataProcessImplV2();
    		res = dt.getListPage(params[0], params[1]);
    		/*int total = 0;
    		while (res == null || (res != null && res.size() == 0))
    		{
    			ex = (System.currentTimeMillis() - time);
    			if (ex >= 4000)
    			{
    				time = System.currentTimeMillis();
    				total ++;
    				res = dt.getListPage(params[0], params[1]);
    			}
    			if (total > 6)
    			{
    				break;
    			}
    		}*/
    		return res;
    	}

    	@Override
    	protected void onPostExecute(List<Page> result) {
    		List<String> urlImages = new ArrayList<String>();
			for (Page page : result)
			{
				urlImages.add(page.getUrlImage());
			}
			if (urlImages.size() == 0){
				Toast.makeText(ImageViewActivity.this, IMangaConst.CHAP_ERROR, Toast.LENGTH_LONG).show();
			}else{
				displayImages(urlImages.toArray(new String[urlImages.size()]), false);
			}
    	}

    	@Override
    	protected void onPreExecute() {
    	}

    	@Override
    	protected void onProgressUpdate(Void... values) {
    	}
	}
	private void displayPager(Bundle savedInstanceState)
	{
		setContentView(R.layout.ac_image_pager);
		/*requestWindowFeature(Window.FEATURE_NO_TITLE);
	     getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
		Intent intent = getIntent();
        
		String downloaded = intent.getStringExtra("downloaded");
        if (downloaded == null)
        {
	        String jsonText = String.valueOf(intent.getStringExtra("url"));
	        new PageOnlineTask().execute(IMangaConst.CONTENT, jsonText);
        }
        else
        {
            SamplePagerAdapter samplePagerAdapter = new SamplePagerAdapter(null);
        	File folder = new File(downloaded);
        	File[] listOfFiles = folder.listFiles();
        	if (listOfFiles.length > 1)
        	{
	        	String[] files = new String[listOfFiles.length - 1];
	        	for (int i = 1; i < listOfFiles.length; i++)
	        	{
	        		files[i-1] = "file:///" + listOfFiles[i].getPath();
	        	}
	        	displayImages(files, true);
        	}
        	/*ViewPager mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
        	samplePagerAdapter.setListDownload(listOfFiles);
    		//setContentView(mViewPager);
    		mViewPager.setAdapter(samplePagerAdapter);*/
        }
	}
	private void displayImages(String[] images, boolean isDownload)
	{
		int pagerPosition = 0;
		
		options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.loading)
			.showImageForEmptyUri(R.drawable.loading)
			.showImageOnFail(R.drawable.loading)
			.resetViewBeforeLoading(true)
			.cacheOnDisc(true)
			.imageScaleType(ImageScaleType.EXACTLY)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.considerExifParams(true)
			.displayer(new FadeInBitmapDisplayer(300))
			.build();
		pager = (ViewPager) findViewById(R.id.pagerAC);
		pager.setAdapter(new ImagePagerAdapter(images, isDownload));
		pager.setCurrentItem(pagerPosition);
	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_POSITION, pager.getCurrentItem());
	}
	private class ImagePagerAdapter extends PagerAdapter {

		private String[] images;
		private LayoutInflater inflater;
		private boolean isDownload;
		ImagePagerAdapter(String[] images, boolean isDownload) {
			this.images = images;
			this.isDownload = isDownload;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return images.length;
		}

		@Override
		public Object instantiateItem(ViewGroup view, int position) {
			View imageLayout = inflater.inflate(R.layout.viewimage, view, false);
			assert imageLayout != null;
			ImageView imageView = (ImageView) imageLayout.findViewById(R.id.iv_photo);
			final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading2);
			ImageLoaderConfiguration ld = ImageLoaderConfiguration.createDefault(getApplicationContext());
			imageLoader.init(ld);
			imageLoader.displayImage(images[position], imageView, options, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					spinner.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					String message = null;
					switch (failReason.getType()) {
						case IO_ERROR:
							message = "Input/Output error";
							break;
						case DECODING_ERROR:
							message = "Image can't be decoded";
							break;
						case NETWORK_DENIED:
							message = "Downloads are denied";
							break;
						case OUT_OF_MEMORY:
							message = "Out Of Memory error";
							break;
						case UNKNOWN:
							message = "Unknown error";
							break;
					}
					Toast.makeText(ImageViewActivity.this, message, Toast.LENGTH_SHORT).show();

					spinner.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					spinner.setVisibility(View.GONE);
				}
			});
			BannerView mBanner = (BannerView) imageLayout.findViewById(R.id.bannerViewpageAC);
			mBanner.getAdSettings().setAdType(AdType.ALL);
			mBanner.getAdSettings().setAdspaceId(65824976);
			mBanner.getAdSettings().setPublisherId(923876207);
			mBanner.asyncLoadNewBanner();
			
			view.addView(imageLayout, 0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		displayPager(savedInstanceState);
	}
}