package vietnamsoft.manga;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.simonvt.menudrawer.MenuDrawer;

import org.apache.commons.lang3.StringUtils;

import vietnamsoft.manga.adapter.PackageAdapter;
import vietnamsoft.manga.adapter.PackageItem;
import vietnamsoft.manga.utl.AppRater;
import vietnamsoft.manga.utl.DataUtil;
import vietnamsoft.manga.utl.IMangaConst;
import vietnamsoft.manga.utl.InternetUtil;
import vietnamsoft.manga.utl.MockUtils;
import vietnamsoft.manga.utl.SettingsManager;
import vietnamsoft.swipelistview.BaseSwipeListViewListener;
import vietnamsoft.swipelistview.SwipeListView;
import vn.vietnamsoft.manga.impl.DataProcessImpl;
import vn.vietnamsoft.manga.impl.DataProcessImplV2;
import vn.vietnamsoft.manga.inteface.IDataProcess;
import vn.vietnamsoft.manga.vo.Manga;
import vn.vietnamsoft.manga.vo.UpdateApp;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class MainActivity extends FragmentActivity {
	EditText search;

	// sideIndex
	private List<Manga> mangas = new ArrayList<Manga>();
	private static final String STATE_ACTIVE_POSITION = "net.simonvt.menudrawer.samples.ContentSample.activePosition";
	private static final String STATE_CONTENT_TEXT = "net.simonvt.menudrawer.samples.ContentSample.contentText";
	private MenuDrawer mMenuDrawer;
	private MenuAdapter mAdapter;
	private ListView mList;
	private int mActivePosition = -1;
	private String mContentText;
	private TextView mContentTextView;
	private static final int REQUEST_CODE_SETTINGS = 0;

	protected static final String TAG = null;
    private PackageAdapter adapter;
    private List<PackageItem> data;
    private List<Manga> mangaHot = new ArrayList<Manga>();
    private List<Manga> mangaNew = new ArrayList<Manga>();
    private List<PackageItem> histories;
    private List<PackageItem> favourites;
    private SwipeListView swipeListView;
    private InternetUtil internetUtil = new InternetUtil();
    public static Map<String, Object> cacheChapter = new LinkedHashMap<String, Object>();
	private ProgressDialog progress;
	private String rootPath = null;
	private String admobId = "";
	private BannerView mBanner = null;
	private String getRootPathStorage()
	{
		String path = null;
		Boolean isSDPresent = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	    if(isSDPresent)
	    {
	      path = Environment.getExternalStorageDirectory().getAbsolutePath();
	    }
	    else
	    {
	        path = getFilesDir().getAbsolutePath();
	    }
	    return path;
	}
	private void loadPushNotification()
	{
		Parse.initialize(this, "9CzbDqoeYtcBCAqUNITugbbaswT9SqhnCv7Oe8gK", "VusDuzecyald1JAZ5B1LkqKrs55M7D0kqCwOZ8eX");
		PushService.setDefaultPushCallback(this, MainActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
		ParseAnalytics.trackAppOpened(getIntent());
	}
	private void showPleaseWait()
	{
//		String temp=stringEncrypt("aaaaaaaabbbbbbbb");
//		Log.v("tdtanvn","tdtanvn"+temp);
		progress = ProgressDialog.show(this, "Đang tải dữ liệu",
        	    "Vui lòng đợi trong giây lát ...", true);
	}
	private void hidePleaseWait()
	{
		if (progress != null)
    	{
    		progress.dismiss();
    		progress = null;
    	}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		internetUtil.setActivity(this);
		showPleaseWait();
		rootPath = getRootPathStorage();
		loadPushNotification();
		histories = new ArrayList<PackageItem>();
		if (savedInstanceState != null) {
			mActivePosition = savedInstanceState.getInt(STATE_ACTIVE_POSITION);
			mContentText = savedInstanceState.getString(STATE_CONTENT_TEXT);
		}
		mMenuDrawer = MenuDrawer.attach(this, MenuDrawer.MENU_DRAG_CONTENT);
		mMenuDrawer.setContentView(R.layout.activity_contentsample);
		mMenuDrawer.setTouchMode(MenuDrawer.TOUCH_MODE_FULLSCREEN);
		mList = new ListView(this);
		mAdapter = new MenuAdapter(generateMenu());
		mList.setAdapter(mAdapter);
		mList.setOnItemClickListener(mItemClickListener);
		mMenuDrawer.setMenuView(mList);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		mContentTextView = (TextView) findViewById(R.id.contentText);
		mContentTextView.setText(mContentText);
		loadListViewWithAutocomplete(IMangaConst.HOST, IMangaConst.ONE);
		//new UpdateAppTask().execute("updateApp");
		/*if (internetUtil.haveNetworkConnection())
		{
			UpdateMangaTask updateMangaTask = new UpdateMangaTask();
			updateMangaTask.setRootPath(rootPath);
			updateMangaTask.execute(IMangaConst.MANGA, IMangaConst.MANGA_ALL_QUERY);
		}*/
	}
	
	private List<Object> generateMenu()
	{
		List<Object> items = new ArrayList<Object>();
		items.add(new Category("Online"));
		items.add(new Item("Tất cả truyện", 0, DataUtil.MangaType.ALL));
//		items.add(new Item("Mới nhất", 0, DataUtil.MangaType.LASTEST));
//		items.add(new Item("Truyện hot", 0, DataUtil.MangaType.HOT));
//		items.add(new Item("Truyện mới cập nhật", 0, DataUtil.MangaType.NEW));
		items.add(new Category("Offline"));
		items.add(new Item("Đã tải", 0, DataUtil.MangaType.DOWNLOADED));
		items.add(new Item("Yêu thích", 0, DataUtil.MangaType.LOVED));
		items.add(new Item("Lịch sử", 0, DataUtil.MangaType.HISTORY));
		//items.add(new Category("Thiết lập"));
		//items.add(new Item("Thiết lập", 0, DataUtil.MangaType.SETTINGS));
		//items.add(new Item("Góp ý", 0, DataUtil.MangaType.CONTACT));
		items.add(new Item("Giới thiệu", 0, DataUtil.MangaType.ABOUT));
		return items;
	}
	private class StandardArrayAdapter extends BaseAdapter implements
			Filterable {

		private final List<Manga> items;

		public StandardArrayAdapter(List<Manga> args) {
			this.items = args;
		}

		@Override
		public View getView(final int position, final View convertView,
				final ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				final LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = vi.inflate(R.layout.row, null);
			}
			TextView textView = (TextView) view.findViewById(R.id.row_title);
			if (textView != null) {
				textView.setText(Html.fromHtml(items.get(position).getName()));
			}
			return view;
		}

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Filter getFilter() {
			Filter listfilter = new MyFilter();
			return listfilter;
		}

		@Override
		public Object getItem(int position) {
			return items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}
	}

	public class MyFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// NOTE: this function is *always* called from a background thread,
			// and
			// not the UI thread.
			constraint = search.getText().toString();
			FilterResults result = new FilterResults();
			if (constraint != null && constraint.toString().length() >= 2) {
				// do not show side index while filter results
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						/*((LinearLayout) findViewById(R.id.list_index))
								.setVisibility(View.INVISIBLE);*/
					}
				});

				List<Manga> filt = new ArrayList<Manga>();
				List<Manga> Items = new ArrayList<Manga>();
				synchronized (this) {
					Items = mangas;
				}
				for (int i = 0; i < Items.size(); i++) {
					String item = Items.get(i).getName();
					if (item.toLowerCase().startsWith(
							constraint.toString().toLowerCase())) {
						filt.add(Items.get(i));
					}
				}

				result.count = filt.size();
				result.values = filt;
			} else {
				synchronized (this) {
					result.count = mangas.size();
					result.values = mangas;
				}

			}
			return result;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			@SuppressWarnings("unchecked")
			List<Manga> filtered = (ArrayList<Manga>) results.values;
			reload();
			doLoadList(filtered);
		}

	}

	private TextWatcher filterTextWatcher = new TextWatcher() {

		public void afterTextChanged(Editable s) {
			new StandardArrayAdapter(mangas).getFilter().filter(
					s.toString());
		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// your search logic here
		}

	};

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.action_contact:
			QuickContactFragment dialog = new QuickContactFragment();
			dialog.show(getSupportFragmentManager(), "QuickContactFragment");
			return true;
		case android.R.id.home:
			mMenuDrawer.toggleMenu();
		}

		return super.onOptionsItemSelected(item);
	}
	private void loadListViewWithAutocomplete(String query, String json) {
		search = (EditText) findViewById(R.id.search_query);
		search.addTextChangedListener(filterTextWatcher);
		File dir = new File(rootPath + "/" + IMangaConst.MANGA24H);
		File manga = new File (rootPath + "/" + IMangaConst.MangaDataPath);
		if (!dir.exists() && !manga.exists())
		{
			//first time load 
			dir.mkdir();
			if (internetUtil.haveNetworkConnection())
			{
				GetMangaOnlineTask getMangaOnlineTask = new GetMangaOnlineTask();
				getMangaOnlineTask.execute(query, json);
			}
			else
			{
				Crouton.makeText(MainActivity.this, IMangaConst.NO_INTERNET, Style.ALERT);
				return;
			}
		}
		else
		{
			GetMangaOfflinelineTask getMangaOfflinelineTask = new GetMangaOfflinelineTask();
			getMangaOfflinelineTask.execute("getoffline");
		}
	}
	private void doLoadList(List<Manga> lst) 
	{
		data = new ArrayList<PackageItem>();
		for (Manga item : lst)
		{
			PackageItem packageItem = new PackageItem();
			packageItem.setName(item.getName());
			packageItem.setPackageName(item.getType());
			packageItem.setUrlImage(item.getImage());
			packageItem.setUrlManga(item.getUrl());
			packageItem.setChapterName(item.getNamechapter());
			packageItem.setId(item.getId());
			data.add(packageItem);
		}
        adapter = new PackageAdapter(this, data);
        adapter.setAdmobId(admobId);
        adapter.setFavouriteList(favourites);
        swipeListView = (SwipeListView) findViewById(R.id.example_lv_list);

        if (Build.VERSION.SDK_INT >= 11) {
            swipeListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            swipeListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

                @Override
                public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                      long id, boolean checked) {
                    mode.setTitle("Selected (" + swipeListView.getCountSelected() + ")");
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    return false;
                }

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    return true;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    swipeListView.unselectedChoiceStates();
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }
            });
        }

        swipeListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
			@Override
            public void onClickFrontView(int position) {
            	showPleaseWait();
            	PackageItem ob = (PackageItem)swipeListView.getItemAtPosition(position);
            	
            	if (internetUtil.haveNetworkConnection() || DataUtil.isFolderExist(rootPath, ob.getName()))
            	{
            		if ((histories != null && histories.size() == 0) || !DataUtil.checkInList(histories, ob))
	            	{
	            		histories.add(ob);
	            	}
	            	Intent myIntent = new Intent(MainActivity.this, IndexableListViewActivity.class);
	            	myIntent.putExtra("selectedManga", ob.getId()); //Optional parameters
	            	myIntent.putExtra("mangaName", ob.getName()); //Optional parameters
	            	myIntent.putExtra("admobId", admobId); //Optional parameters
	            	MainActivity.this.startActivity(myIntent);
            	}
            	else
            	{
					Crouton.makeText(MainActivity.this, IMangaConst.NO_INTERNET, Style.ALERT).show();
            	}
            	hidePleaseWait();
            }
            @Override
            public void onDismiss(int[] reverseSortedPositions) {
                for (int position : reverseSortedPositions) {
                    data.remove(position);
                }
                adapter.notifyDataSetChanged();
            }

        });
        reload();
        swipeListView.setAdapter(adapter);
	}

	private void loadAdview(String adId)
	{
		mBanner = (BannerView) findViewById(R.id.bannerView);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(65824976);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_ACTIVE_POSITION, mActivePosition);
		outState.putString(STATE_CONTENT_TEXT, mContentText);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

	@SuppressWarnings("unchecked")
	private void displayMangaType(Item item) {
		switch (item.menuType) {
		case HOT:
			showPleaseWait();
			if (mangaHot == null || (mangaHot != null && mangaHot.size() == 0))
			{
				try {
					mangaHot = (List<Manga>)MockUtils.readTestdatafromFileSystem(rootPath + IMangaConst.MangaHotDataPath);
					doLoadList(mangaHot);
					hidePleaseWait();
				} catch (FileNotFoundException e) {
					if (internetUtil.haveNetworkConnection())
					{
						new GetMangaHotOnlineTask().execute(IMangaConst.MANGA,IMangaConst.MANGA_HOT_QUERY);
					}
					else
					{
						Crouton.makeText(MainActivity.this, IMangaConst.NO_INTERNET, Style.ALERT);
					}
				}
			}
			else
			{
	
				doLoadList(mangaHot);
				hidePleaseWait();
			}
			break;
		case NEW:
			if (mangaNew != null && mangaNew.size() > 0)
			{
				showPleaseWait();
				doLoadList(mangaNew);
				hidePleaseWait();
			}
			else
			{
				if (internetUtil.haveNetworkConnection())
				{
					showPleaseWait();
					new GetMangaNewOnlineTask().execute(IMangaConst.MANGA, IMangaConst.MANGA_NEW_QUERY);
				}
				else
				{
					Crouton.makeText(MainActivity.this, IMangaConst.NO_INTERNET, Style.ALERT);
				}
			}
			break;
		case LASTEST:
			Toast.makeText(this, "Lastest manga", Toast.LENGTH_SHORT).show();
			break;
		case ALL:
			Toast.makeText(this, "All manga", Toast.LENGTH_SHORT).show();
			if(mangas != null)
			{
				doLoadList(mangas);
				mContentTextView.setText("Tất cả truyện (" + mangas.size() + ")");
			}
			break;
		case DOWNLOADED:
			File downloaded = new File(rootPath + "/manga24h/");
			int count = 0;
			if (downloaded.exists())
			{
				List<Manga> downloadedManga = new ArrayList<Manga>();
				for (File f : downloaded.listFiles())
				{
					if (f.isDirectory())
					{
						String name = f.getName();
						boolean findAll = false;
						for (Manga mg : mangas) {
							if (mg.getName().equals(name)) {
								downloadedManga.add(mg);
								findAll = true;
								break;
							}
						}
						if (!findAll && mangaHot != null)
						{
							for (Manga mg : mangaHot) {
								if (mg.getName().equals(name)) {
									downloadedManga.add(mg);
									findAll = true;
									break;
								}
							}
						}
					}
				}
				count = downloadedManga.size();
				doLoadList(downloadedManga);
			}
			mContentTextView.setText("Truyện đã tải (" + count + ")");
			break;
		case CONTACT:
			Toast.makeText(this, "Contact dev", Toast.LENGTH_SHORT).show();
			break;
		case LOVED:
			Toast.makeText(this, "loved manga", Toast.LENGTH_SHORT).show();
			if (favourites == null)
    		{
				favourites = new ArrayList<PackageItem>();
    		}
			reload();
	        adapter = new PackageAdapter(this, favourites);
	        adapter.setFavouriteList(favourites);
	        swipeListView.setAdapter(adapter);
	        mContentTextView.setText("Truyện yêu thích (" + favourites.size() + ")");
			break;
		case HISTORY:
			Toast.makeText(this, "history manga", Toast.LENGTH_SHORT).show();
			reload();
	        adapter = new PackageAdapter(this, histories);
	        adapter.setFavouriteList(favourites);
	        swipeListView.setAdapter(adapter);
	        mContentTextView.setText("Lịch sử xem (" + histories.size() + ")");
			break;
		case ABOUT:
			QuickContactFragment dialog = new QuickContactFragment();
			dialog.show(getSupportFragmentManager(), "QuickContactFragment");
			break;
		default:
			break;
		}
	}

	private AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			mActivePosition = position;
			mMenuDrawer.setActiveView(view, position);

			mContentTextView.setText(((TextView) view).getText());
			mMenuDrawer.closeMenu();
			loadAdview(admobId);
			/*AdView adView = (AdView) findViewById(R.id.adView);
			AdRequest adRequest = new AdRequest();
			adRequest.addTestDevice("C9FBE0516AF940E785C93575F7DD1D10");
			adView.loadAd(adRequest);*/
			ListView lv = (ListView) parent;
			lv.getItemAtPosition(position);
			Item currentItem = (MainActivity.Item) lv
					.getItemAtPosition(position);
			displayMangaType(currentItem);
		}
	};
	private static long back_pressed;
	@Override
	public void onBackPressed() {
		final int drawerState = mMenuDrawer.getDrawerState();
		if (drawerState == MenuDrawer.STATE_OPEN
				|| drawerState == MenuDrawer.STATE_OPENING) {
			mMenuDrawer.closeMenu();
			return;
		}
		try {
			if (favourites != null)
			{
				MockUtils.writeTestdataToFileSystem(rootPath + IMangaConst.FavouriteDataPath, favourites);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (back_pressed + 2000 > System.currentTimeMillis())
		{
			super.onBackPressed();
		}
		else
		{
			Toast.makeText(this, IMangaConst.DOUBLE_TO_CLOSE, Toast.LENGTH_SHORT).show();
			back_pressed = System.currentTimeMillis();
		}
	}

	private static class Item {

		String mTitle;
		int mIconRes;
		DataUtil.MangaType menuType;

		Item(String title, int iconRes, DataUtil.MangaType prMenuType) {
			mTitle = title;
			mIconRes = iconRes;
			menuType = prMenuType;
		}
	}

	private static class Category {

		String mTitle;

		Category(String title) {
			mTitle = title;
		}
	}

	private class MenuAdapter extends BaseAdapter {

		private List<Object> mItems;

		MenuAdapter(List<Object> items) {
			mItems = items;
		}

		@Override
		public int getCount() {
			return mItems.size();
		}

		@Override
		public Object getItem(int position) {
			return mItems.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public int getItemViewType(int position) {
			return getItem(position) instanceof Item ? 0 : 1;
		}

		@Override
		public int getViewTypeCount() {
			return 2;
		}

		@Override
		public boolean isEnabled(int position) {
			return getItem(position) instanceof Item;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			Object item = getItem(position);

			if (item instanceof Category) {
				if (v == null) {
					v = getLayoutInflater().inflate(R.layout.menu_row_category,
							parent, false);
				}

				((TextView) v).setText(((Category) item).mTitle);

			} else {
				if (v == null) {
					v = getLayoutInflater().inflate(R.layout.menu_row_item,
							parent, false);
				}

				TextView tv = (TextView) v;
				tv.setText(((Item) item).mTitle);
				tv.setCompoundDrawablesWithIntrinsicBounds(
						((Item) item).mIconRes, 0, 0, 0);
			}

			v.setTag(R.id.mdActiveViewPosition, position);

			if (position == mActivePosition) {
				mMenuDrawer.setActiveView(v, position);
			}

			return v;
		}
	}
	private void reload() {
        SettingsManager settings = SettingsManager.getInstance();
        if (swipeListView != null)
        {
	        swipeListView.setSwipeActionLeft(settings.getSwipeActionLeft());
	        swipeListView.setSwipeActionRight(settings.getSwipeActionRight());
	        swipeListView.setOffsetLeft(convertDpToPixel(settings.getSwipeOffsetLeft()));
	        swipeListView.setOffsetRight(convertDpToPixel(settings.getSwipeOffsetRight()));
	        swipeListView.setAnimationTime(settings.getSwipeAnimationTime());
	        swipeListView.setSwipeOpenOnLongPress(settings.isSwipeOpenOnLongPress());
        }
    }

    public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }

    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_SETTINGS:
                reload();
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	class GetMangaOnlineTask extends AsyncTask<String, Void, List<Manga>>{
		@Override
		protected List<Manga> doInBackground(String... params) {
			IDataProcess dt = new DataProcessImplV2();
			long time = System.currentTimeMillis();
    		long ex = 0;
    		List<Manga> res = new ArrayList<Manga>();
    		res = dt.getListManga(params[0], params[1]);
    		int total = 0;
    		while (res == null || (res != null && res.size() == 0))
    		{
    			ex = (System.currentTimeMillis() - time);
    			if (ex >= 4000)
    			{
    				time = System.currentTimeMillis();
    				total ++;
    				res = dt.getListManga(params[0], params[1]);
    			}
    			if (total > 5)
    			{
    				break;
    			}
    		}
    		if ((res == null || (res != null && res.size() == 0)) && total > 5)
    		{
    			Crouton.makeText(MainActivity.this, IMangaConst.SERVER_BUSY, Style.INFO);
    		}
    		return res;
		}

		@Override
		protected void onPostExecute(List<Manga> result) {
			favourites = new ArrayList<PackageItem>();
			if (result == null)
			{
				result = new ArrayList<Manga>();
				Crouton.makeText(MainActivity.this, IMangaConst.NO_INTERNET, Style.ALERT);
			}
			mangas = result;
			doLoadList(result);
			mContentTextView.setText("Tất cả truyện (" + result.size() + ")");
			try {
				MockUtils.writeTestdataToFileSystem(rootPath + IMangaConst.MangaDataPath, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
			hidePleaseWait();
		}
	}
	class GetMangaOfflinelineTask extends AsyncTask<String, Void, List<Manga>>{
		@SuppressWarnings("unchecked")
		@Override
		protected List<Manga> doInBackground(String... params) {
			try {
				File hot = new File (rootPath + IMangaConst.MangaHotDataPath);
				if (hot.exists())
				{
					mangaHot = (ArrayList<Manga>)MockUtils.readTestdatafromFileSystem(rootPath + IMangaConst.MangaHotDataPath);
				}
				else
				{
					mangaHot = new ArrayList<Manga>();
				}
				File likes = new File (rootPath + IMangaConst.FavouriteDataPath);
				if (likes.exists())
				{
					favourites = (ArrayList<PackageItem>)MockUtils.readTestdatafromFileSystem(rootPath + IMangaConst.FavouriteDataPath);
				}
				else
				{
					favourites = new ArrayList<PackageItem>();
				}
				return (ArrayList<Manga>)MockUtils.readTestdatafromFileSystem(rootPath + IMangaConst.MangaDataPath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			catch (Exception e) {
				Toast.makeText(MainActivity.this, IMangaConst.SERVER_BUSY, Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
			catch (Error e) {
				Toast.makeText(MainActivity.this, IMangaConst.SERVER_BUSY, Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(List<Manga> result) {
			if (result == null)
			{
				result = new ArrayList<Manga>();
				Crouton.makeText(MainActivity.this, IMangaConst.NO_INTERNET, Style.ALERT);
			}
			mangas = result;
			doLoadList(result);
			mContentTextView.setText("Tất cả truyện (" + result.size() + ")");
			hidePleaseWait();
		}
	}
	class GetMangaNewOnlineTask extends AsyncTask<String, Void, List<Manga>>{
		@Override
		protected List<Manga> doInBackground(String... params) {
			IDataProcess dt = new DataProcessImpl();
			long time = System.currentTimeMillis();
    		long ex = 0;
    		List<Manga> res = new ArrayList<Manga>();
    		res = dt.getListManga(params[0], params[1]);
    		int total = 0;
    		while ((res == null || (res != null && res.size() == 0)))
    		{
    			ex = (System.currentTimeMillis() - time);
    			if (ex >= 4000)
    			{
    				time = System.currentTimeMillis();
    				total ++;
    				res = dt.getListManga(params[0], params[1]);
    			}
    			if (total > 5)
    			{
    				break;
    			}
    		}
    		if ((res == null || (res != null && res.size() == 0)) && total > 5)
    		{
    			Crouton.makeText(MainActivity.this, IMangaConst.SERVER_BUSY, Style.INFO);
    		}
    		System.out.println(System.currentTimeMillis() - time);
    		return res;
		}
		
		@Override
		protected void onPostExecute(List<Manga> result) {
			if ((result == null || (result != null && result.size() == 0)))
			{
				result = new ArrayList<Manga>();
				Crouton.makeText(MainActivity.this, IMangaConst.NO_INTERNET_OR_BUSY, Style.ALERT);
			}
			mangaNew = result;
			doLoadList(result);
			mContentTextView.setText("Truyện mới cập nhật (" + result.size() + ")");
			hidePleaseWait();
		}
	}
	class GetMangaHotOnlineTask extends AsyncTask<String, Void, List<Manga>>{
		@Override
		protected List<Manga> doInBackground(String... params) {
			IDataProcess dt = new DataProcessImpl();
			long time = System.currentTimeMillis();
    		long ex = 0;
    		List<Manga> res = new ArrayList<Manga>();
    		res = dt.getListManga(params[0], params[1]);
    		int total = 0;
    		while (res == null || (res != null && res.size() == 0))
    		{
    			ex = (System.currentTimeMillis() - time);
    			if (ex >= 4000)
    			{
    				time = System.currentTimeMillis();
    				total ++;
    				res = dt.getListManga(params[0], params[1]);
    			}
    			if (total > 5)
    			{
    				break;
    			}
    		}
    		if ((res == null || (res != null && res.size() == 0)) && total > 5)
    		{
    			Crouton.makeText(MainActivity.this, IMangaConst.SERVER_BUSY, Style.INFO);
    		}
    		System.out.println(System.currentTimeMillis() - time);
    		return res;
		}
		
		@Override
		protected void onPostExecute(List<Manga> result) {
			if (result == null)
			{
				result = new ArrayList<Manga>();
				Crouton.makeText(MainActivity.this, IMangaConst.NO_INTERNET, Style.ALERT);
			}
			mangaHot = result;
			doLoadList(result);
			mContentTextView.setText("Truyện hot (" + result.size() + ")");
			try {
				MockUtils.writeTestdataToFileSystem(rootPath + IMangaConst.MangaHotDataPath, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
			hidePleaseWait();
		}
	}
	private void openPlayStore(String url)
	{
		String appPackageName = getPackageName();
		if (StringUtils.isNotEmpty(url))
		{
			appPackageName = url;
		}
		try {
		    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
		    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
		}
	}
	private void showUpdateApp(String type, String message, final String url)
	{
		if (StringUtils.isEmpty(type))
		{
			return;
		}
		AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
		alertDialog.setTitle("Đã có phiên bản mới trên Google Play");
		alertDialog.setMessage(message);
		if (type.trim().equals("YES|NO"))
		{
			alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
				}
			});
			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					openPlayStore(url);
				}
			});
		}
		if (type.trim().equals("YES"))
		{
			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					openPlayStore(url);
				}
			});
		}
		alertDialog.show();
	}
	class UpdateAppTask extends AsyncTask<String, Void, UpdateApp>{
		@Override
		protected UpdateApp doInBackground(String... params) {
			if (internetUtil.haveNetworkConnection())
			{
				IDataProcess dt = new DataProcessImpl();
				return dt.getUpdateApp();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(UpdateApp result) {
			if (result != null)
			{
				admobId = DataUtil.getAdmobId(result.getAdmobId());
				loadAdview(admobId);
				try {
					MockUtils.writeTestdataToFileSystem(rootPath + IMangaConst.METADataPath, result);
				} catch (Exception e) {
					e.printStackTrace();
				}
				showUpdateApp(result.getType(), result.getContent(), result.getUrl());
				AppRater.setPackage(result.getUrl());
				AppRater.app_launched(MainActivity.this);
			}
			else
			{
				loadAdview(admobId);
			}
		}
		
		@Override
		protected void onPreExecute() {
			try {
				File file = new File(rootPath + IMangaConst.METADataPath);
				if (file.exists())
				{
					UpdateApp offline = (UpdateApp) MockUtils.readTestdatafromFileSystem(rootPath + IMangaConst.METADataPath);
					admobId = DataUtil.getAdmobId(offline.getAdmobId());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}