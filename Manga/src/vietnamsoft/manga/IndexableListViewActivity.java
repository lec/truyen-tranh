package vietnamsoft.manga;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import vietnamsoft.asynctask.DownloadTask;
import vietnamsoft.manga.utl.IMangaConst;
import vietnamsoft.manga.utl.InternetUtil;
import vietnamsoft.manga.widget.IndexableListView;
import vn.vietnamsoft.manga.impl.DataProcessImplV2;
import vn.vietnamsoft.manga.inteface.IDataProcess;
import vn.vietnamsoft.manga.util.Base64;
import vn.vietnamsoft.manga.vo.Chapter;
import vn.vietnamsoft.manga.vo.Page;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public class IndexableListViewActivity extends Activity {
	protected static final String TAG = null;
	private List<String> mItems;
	private List<Chapter> chapters;
	private IndexableListView mListView;
	public static boolean isDownloading = false;
	private InternetUtil internetUtil = new InternetUtil();
	private String mangaName = "";
	int position = 0;
	private ProgressDialog progress;
	private String rootPath;
	private DownloadTask dl;
	private String admobId = "";

	// ...

	private PullToRefreshLayout mPullToRefreshLayout;
    /** Called when the activity is first created. */
    @SuppressWarnings("unchecked")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.indexablelistview);
        mListView = (IndexableListView) findViewById(R.id.listview);
        showPleaseWait();
		internetUtil.setActivity(this);
		rootPath = getRootPathStorage();
        Intent intent = getIntent();
        int mangaId = intent.getIntExtra("selectedManga",0);
        mangaName = intent.getStringExtra("mangaName");
        admobId = intent.getStringExtra("admobId");
        loadAdview(admobId);
        mItems = new ArrayList<String>();
        //chapters = new ArrayList<Chapter>();
        Md5FileNameGenerator md5 = new Md5FileNameGenerator();
        if (MainActivity.cacheChapter != null)
        {
	        chapters = MainActivity.cacheChapter.get(md5.generate(mangaName)) != null ? (ArrayList<Chapter>)MainActivity.cacheChapter.get(md5.generate(mangaName)) : new ArrayList<Chapter>();
	        if (chapters != null && chapters.size() > 0)
	        {
	        	for (Chapter chapter : chapters)
				{
					if (checkDownloaded(mangaName, chapter.getName()))
					{
						mItems.add("[Downloaded] " + chapter.getName());
					}
					else
					{
						mItems.add(chapter.getName());
					}
				}
	        	doLoadChapters();
	        	hidePleaseWait();
	        	return;
	        }
        }
        if (!internetUtil.haveNetworkConnection())
        {
        	File downloaded = new File(rootPath + "/manga24h/" + mangaName + "/");
        	if (downloaded.exists() && downloaded.isDirectory())
        	{
        		for (File f : downloaded.listFiles())
        		{
        			if (f.isDirectory())
        			{
        				mItems.add("[Downloaded] " + new String(Base64.decode(f.getName())));
        			}
        		}
        	}
        	doLoadChapters();
        	hidePleaseWait();
        }
        else
        {
	        ChapterOnlineTask chapterOnlineTask = new ChapterOnlineTask();
	        chapterOnlineTask.setMangaName(mangaName);
	        chapterOnlineTask.execute(IMangaConst.CHAPTER, String.valueOf(mangaId));
        }
    }
    @Override
	public void onBackPressed() {
    	if (isDownloading)
    	{
    		Toast.makeText(this, "Đang tải truyện ...", Toast.LENGTH_SHORT).show();
			return;
    	}
    	super.onBackPressed();
	}
    @Override
    public void onDestroy()
    {
        getIntent().removeExtra("selectedManga");
        getIntent().removeExtra("mangaName");
        getIntent().removeExtra("admobId");
        mItems = null;
        chapters = null;
        super.onDestroy();
    }
    
	private String getRootPathStorage()
	{
		String path = null;
		Boolean isSDPresent = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	    if(isSDPresent)
	    {
	      path = Environment.getExternalStorageDirectory().getAbsolutePath();
	    }
	    else
	    {
	        path = getFilesDir().getAbsolutePath();
	    }
	    return path;
	}
	private boolean checkDownloaded(String mangaName, String chapter)
	{
		chapter = Base64.encode(chapter.getBytes());
		boolean bool = false;
		String pathDownloaded = rootPath + "/manga24h/"+ mangaName + "/" + chapter + "/";
		File fileDownload = new File(pathDownloaded);
		if (fileDownload.exists() && fileDownload.listFiles() != null && fileDownload.listFiles().length > 0)
		{
			bool = true;
		}
		return bool;
	}
	private void doLoadChapters()
	{
		ContentAdapter adapter = new ContentAdapter(IndexableListViewActivity.this,
                android.R.layout.simple_list_item_1, mItems);
        mListView.setAdapter(adapter);
        mListView.setFastScrollEnabled(true);
        mListView.setLongClickable(true);
       
        mListView.setOnItemLongClickListener(new OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                    int pos, long id) {
            	final TextView tv = (TextView)arg1;
            	if (internetUtil.haveNetworkConnection() == false || isDownloading ||  tv.getText().toString().startsWith("[Downloaded]"))
            	{
            		return true;
            	}
            	position = pos;
            	
            	AlertDialog alertDialog = new AlertDialog.Builder(IndexableListViewActivity.this).create();
				alertDialog.setTitle("Tải truyện : " + mangaName );
				alertDialog.setMessage("Bạn có muốn tải chap này ?");
				alertDialog.setIcon(R.drawable.icon_manga);
				alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						arg0.dismiss();
					}
				});
				alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				// here you can add functions
					dialog.dismiss();
					Crouton.makeText(IndexableListViewActivity.this, "Đang tải xuống máy " + mangaName, Style.INFO).show();
					String textSelected = tv.getText().toString();
					List<String> items = new ArrayList<String>();
					for (String item : mItems)
					{
						if (item.equals(textSelected))
						{
							items.add("[Downloading] " + item);
						}
						else
						{
							items.add(item);
						}
					}
					mItems = items;
					ContentAdapter adapter = new ContentAdapter(IndexableListViewActivity.this,
			                android.R.layout.simple_list_item_1, items);
					mListView.setAdapter(adapter);
					tv.setText("[Downloading] " + tv.getText());
					String chapId = String.valueOf(chapters.get(position).getId());
					PageOnlineTask pageOnlineTask = new PageOnlineTask();
					pageOnlineTask.execute(IMangaConst.CONTENT, chapId);
				}
				});
				alertDialog.setIcon(R.drawable.android_logo);
				alertDialog.show();
                return true;
            }
        }); 
        mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String name = mItems.get(arg2);
				boolean isDownloading = name.startsWith("[Downloading]");
				if (isDownloading)
				{
					return;
				}
				showPleaseWait();
				boolean isDownloaded = name.startsWith("[Downloaded]");
				if (!isDownloaded && internetUtil.haveNetworkConnection())
				{
					Chapter chapter = chapters.get(arg2);
					Intent myIntent = new Intent(IndexableListViewActivity.this, ImageViewActivity.class);
					if (checkDownloaded(mangaName, chapter.getName()))
					{
						myIntent.putExtra("downloaded", rootPath + "/manga24h/"+ mangaName + "/" + Base64.encode(chapter.getName().getBytes()) + "/"); //Optional parameters
					}
					myIntent.putExtra("admobId", admobId);
	            	myIntent.putExtra("url", chapter.getId()); //Optional parameters
	            	IndexableListViewActivity.this.startActivity(myIntent);
				}
				else
				{
					if (isDownloaded)
					{
						name = name.replace("[Downloaded] ", "");
						if (checkDownloaded(mangaName, name))
						{
							Intent myIntent = new Intent(IndexableListViewActivity.this, ImageViewActivity.class);
							myIntent.putExtra("downloaded", rootPath + "/manga24h/"+ mangaName + "/" + Base64.encode(name.getBytes()) + "/"); //Optional parameters
							myIntent.putExtra("admobId", admobId);
							IndexableListViewActivity.this.startActivity(myIntent);
						}
					}
					else
					{
						Crouton.showText(IndexableListViewActivity.this, IMangaConst.NO_INTERNET, Style.ALERT);
					}
				}
				hidePleaseWait();
			}

            
        });
	}
	private void showPleaseWait()
	{
		progress = ProgressDialog.show(this, "Đang tải dữ liệu",
        	    "Vui lòng đợi trong giây lát ...", true);
	}
	
	private void hidePleaseWait()
	{
		if (progress != null)
		{
			progress.dismiss();
			progress = null;
		}
	}
    
    private class ContentAdapter extends ArrayAdapter<String> {
    	
		public ContentAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
		}
    }
    class ChapterOnlineTask extends AsyncTask<String, Void, List<Chapter>>{
    	private String mangaName;
    	public void setMangaName(String name)
    	{
    		mangaName = name;
    	}
    	@Override
    	protected List<Chapter> doInBackground(String... params) {
    		List<Chapter> res = new ArrayList<Chapter>();
    		IDataProcess dt = new DataProcessImplV2();
    		res = dt.getListChapter(params[0], params[1]);
    		if ((res == null || (res != null && res.size() == 0)))
    		{
    			Crouton.makeText(IndexableListViewActivity.this, IMangaConst.SERVER_BUSY, Style.INFO);
    		}
    		return res;
    	}

    	@Override
    	protected void onPostExecute(List<Chapter> result) {
    		if (result != null && result.size() > 0)
			{
				for (Chapter chapter : result)
				{
					if (checkDownloaded(mangaName, chapter.getName()))
					{
						mItems.add("[Downloaded] " + chapter.getName());
					}
					else
					{
						mItems.add(chapter.getName());
					}
					chapters.add(chapter);
				}
			}
    		if (MainActivity.cacheChapter != null && MainActivity.cacheChapter.size() < 30)
    		{
    			Md5FileNameGenerator md5 = new Md5FileNameGenerator();
    			MainActivity.cacheChapter.put(md5.generate(mangaName), result);
    		}
    		doLoadChapters();
    		hidePleaseWait();
    		if (mPullToRefreshLayout != null)
    		{
    			mPullToRefreshLayout.setRefreshComplete();
    		}
    	}

    	@Override
    	protected void onPreExecute() {
    	}

    	@Override
    	protected void onProgressUpdate(Void... values) {
    	}
    }
    class PageOnlineTask extends AsyncTask<String, Void, List<Page>>{
    	@Override
    	protected List<Page> doInBackground(String... params) {
    		List<Page> res = new ArrayList<Page>();
    		IDataProcess dt = new DataProcessImplV2();
    		res = dt.getListPage(params[0], params[1]);
    		if ((res == null || (res != null && res.size() == 0)))
    		{
    			Crouton.makeText(IndexableListViewActivity.this, IMangaConst.SERVER_BUSY, Style.ALERT);
    		}
    		return res;
    	}

    	@Override
    	protected void onPostExecute(List<Page> result) {
    		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
	        .cacheOnDisc(true)
	        .build();
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
			        .defaultDisplayImageOptions(defaultOptions)
			        .build();
			ImageLoader img = ImageLoader.getInstance();
			img.init(config);
			String root = rootPath + "/manga24h/" + mangaName;
			File folder = new File(root);
			if (!folder.exists())
			{
				folder.mkdir();
			}
			String chapterFolder = root + "/" + Base64.encode(chapters.get(position).getName().getBytes());
			folder = new File(chapterFolder);
			if (!folder.exists())
			{
				folder.mkdir();
			}
			dl = new DownloadTask();
	    	dl.setChapterFolder(chapterFolder);
	    	dl.setContext(IndexableListViewActivity.this);
	    	isDownloading = true;
	    	dl.execute(result);
    	}

    	@Override
    	protected void onPreExecute() {
    	}

    	@Override
    	protected void onProgressUpdate(Void... values) {
    	}
    }

	private void loadAdview(String adId) {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlist);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(65824976);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}

	/*@Override
	public void onRefreshStarted(View view) {
		showPleaseWait();
		String manga = getIntent().getStringExtra("selectedManga");
		String jsonText = "{\"url\":\"" + manga + "\"}";
        ChapterOnlineTask chapterOnlineTask = new ChapterOnlineTask();
        chapterOnlineTask.setMangaName(mangaName);
        chapterOnlineTask.execute("chapter", jsonText);
	}*/
}