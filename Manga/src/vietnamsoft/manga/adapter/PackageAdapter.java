package vietnamsoft.manga.adapter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;


import vietnamsoft.manga.IndexableListViewActivity;
import vietnamsoft.manga.R;
import vietnamsoft.manga.utl.DataUtil;
import vietnamsoft.swipelistview.SwipeListView;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;


public class PackageAdapter extends BaseAdapter {

    private List<PackageItem> data;
    private String admobId;
    private Context context;
    public List<PackageItem> favouteriteList;
    public void setAdmobId(String admobid)
    {
    	this.admobId = admobid;
    }
    public void setFavouriteList(List<PackageItem> frFavouteriteList)
    {
    	favouteriteList = frFavouteriteList;
    }
    public List<PackageItem> getFavouriteList()
    {
    	return favouteriteList;
    }
    public PackageAdapter(Context context, List<PackageItem> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public PackageItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final PackageItem item = getItem(position);
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.package_row, parent, false);
            holder = new ViewHolder();
            holder.ivImage = (ImageView) convertView.findViewById(R.id.example_row_iv_image);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.example_row_tv_title);
            holder.read = (Button) convertView.findViewById(R.id.example_row_b_action_1);
            holder.like = (Button) convertView.findViewById(R.id.xButton);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String mangaName = "<b>" + item.getName() + "</b>";
        ((SwipeListView)parent).recycle(convertView, position);
        UrlImageViewHelper.setUrlDrawable(holder.ivImage, item.getUrlImage(), R.drawable.image_not_available);
        holder.tvTitle.setText(Html.fromHtml(mangaName));
        if(favouteriteList != null && DataUtil.checkInList(favouteriteList,item))
        {
        	holder.like.setBackgroundResource(R.drawable.dislike);
        }
        if (StringUtils.isNotEmpty(item.getChapterName()))
        {
        	 holder.tvTitle.setText(Html.fromHtml(mangaName + " <br/> <i>" + item.getChapterName() + "</i>"));
        }
        //read manga
        holder.read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent myIntent = new Intent(context, IndexableListViewActivity.class);
            	myIntent.putExtra("selectedManga", item.getUrlManga()); //Optional parameters
            	myIntent.putExtra("mangaName", item.getName()); //Optional parameters
            	myIntent.putExtra("admobId", admobId); //Optional parameters
            	context.startActivity(myIntent);
            }
        });
        //bookmark manga
        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            @TargetApi(14)
            public void onClick(View v) {
                if (favouteriteList != null && !DataUtil.checkInList(favouteriteList,item))
                {
                	favouteriteList.add(item);
                	Toast.makeText(context, "Đã thêm vào yêu thích", Toast.LENGTH_SHORT).show();
                	holder.like.setBackgroundResource(R.drawable.dislike);
                }
                else
                {
                	if (favouteriteList == null)
                	{
                		favouteriteList = new ArrayList<PackageItem>();
                		favouteriteList.add(item);
                		Toast.makeText(context, "Đã thêm vào yêu thích", Toast.LENGTH_SHORT).show();
                		holder.like.setBackgroundResource(R.drawable.dislike);
                	}
                	else
                	{
                		favouteriteList.remove(item);
                		holder.like.setBackgroundResource(R.drawable.like);
                		Toast.makeText(context, "Đã hủy yêu thích", Toast.LENGTH_SHORT).show();
                	}
                }
            }
        });
        if ((position % 2) == 0)
        {
        	RelativeLayout rl = (RelativeLayout)convertView.findViewById(R.id.front);
        	rl.setBackgroundResource(R.color.highlightColor);
        }
        
        return convertView;
    }
    static class ViewHolder {
        ImageView ivImage;
        TextView tvTitle;
        TextView tvDescription;
        Button read;
        Button like;
    }

}
