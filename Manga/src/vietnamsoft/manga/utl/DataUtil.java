package vietnamsoft.manga.utl;

import java.io.File;
import java.util.List;

import vietnamsoft.manga.adapter.PackageItem;
import vn.vietnamsoft.manga.util.MyAES;

public class DataUtil {
	public static enum MangaType{
		HOT, NEW, ABOUT, CONTACT, DOWNLOADED, SETTINGS, LASTEST, ALL, HISTORY, LOVED
	}
	public static String getFirstLetterUpperString(String st, int position)
	{
		String rs = "";
		for(int c=position;c<st.length();c++)
		{
			if(Character.isLetter(st.charAt(c)))
			{
				rs = Character.toString(st.charAt(c)).toUpperCase();
				break;
			}
		}
		return rs;
	}
	public static String getAdmobId(String base64Encoded)
	{
		String txt = "";
		try {
			txt = MyAES.decrypt(base64Encoded);
			txt = txt.substring(0, txt.indexOf('/'));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return txt;
	}
	public static boolean isFolderExist(String root, String folder)
	{
		File file = new File (root + "/manga24h/" + folder);
		return file.exists() && file.isDirectory();
	}

	public static boolean checkInList(List<PackageItem> lst, PackageItem item) {
		if (lst == null || (lst != null && lst.size() == 0))
			return false;
		for (PackageItem ob : lst) {
			if (item.getName().equals(ob.getName())) {
				return true;
			}
		}
		return false;
	}
	public static boolean checkInList(List<PackageItem> lst, String item) {
		boolean bool = false;
		for (PackageItem ob : lst) {
			if (item.equals(ob.getName())) {
				bool = true;
				return bool;
			}
		}
		return bool;
	}
}
