/*
 * MockUtils.java
 *
 * Copyright by Orell F�ssli Wirtschaftsinformationen AG
 * Z�rich
 * All rights reserved.
 */
package vietnamsoft.manga.utl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;

import org.apache.commons.lang3.SerializationUtils;

/**
 * Mock for testing of-elasticsearch-bridge
 *
 * @author Ngqhuy
 * @version $Revision: 1.1 $
 */
public final class MockUtils
{
    /**
     * Constructor
     *
     */
    private MockUtils()
    {
    }
    
    /**
     * Writes the given object to the filesystem
     *
     * @param fileName
     * @param objectToWrite
     * @throws Exception 
     */
    public static void writeTestdataToFileSystem(String fileName, Object objectToWrite) throws Exception
    {
        byte[] data = SerializationUtils.serialize((Serializable)objectToWrite);
        FileOutputStream fos = new FileOutputStream(fileName);
        fos.write(data);
        fos.close();
    }
    
    /**
     * Reads the file from disk and returns as java object 
     *
     * @param fileName
     * @return Object
     * @throws FileNotFoundException 
     */
    public static Object readTestdatafromFileSystem(String fileName) throws FileNotFoundException
    {
        FileInputStream fis = null;
        fis = new FileInputStream(fileName);
        return SerializationUtils.deserialize(fis);
    }
}


/*
 * Changes:
 * $Log: MockUtils.java,v $
 * Revision 1.1  2013/08/29 04:16:37  ngh
 * Initiating MockUtils for library
 *
 */