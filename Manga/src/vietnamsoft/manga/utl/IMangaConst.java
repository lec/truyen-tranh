package vietnamsoft.manga.utl;

public interface IMangaConst {
	public static String HOST = "http://phimhdplus.com/manga24h/api";
	public static String ONE = "1";
	public static String CHAPTER = "http://phimhdplus.com/manga24h/api/chapter";
	public static String CONTENT = "http://phimhdplus.com/manga24h/api/content";
	public static String MANGA24H = "manga24h";
	public static String MANGA = "manga";
	public static String MangaDataPath = "/manga24h/manga.dat";
	public static String MangaHotDataPath = "/manga24h/mangahot.dat";
	public static String FavouriteDataPath = "/manga24h/favourites.dat";
	public static String METADataPath = "/manga24h/meta.dat";
	public static String NO_INTERNET = "Không có kết nối internet";
	public static String NO_INTERNET_OR_BUSY = "Server đang bận, xin vui lòng thử lại lúc khác";
	public static String DOUBLE_TO_CLOSE = "Bấm thêm lần nữa để đóng ứng dụng !";
	public static String SERVER_BUSY = "Server đang bận, xin vui lòng thử lại sau ít phút!";
	public static String CHAP_ERROR = "Hiện tại chap này chưa sẵn sàng, server đang cập nhật";
	public static String MANGA_ALL_QUERY =  "{\"type\": 1, \"page\": 0}";
	public static String MANGA_HOT_QUERY =  "{\"type\": 2, \"page\": 0}";
	public static String MANGA_NEW_QUERY =  "{\"type\": 3, \"page\": 0}";
}
