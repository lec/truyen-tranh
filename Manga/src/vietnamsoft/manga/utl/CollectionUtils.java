package vietnamsoft.manga.utl;

import java.util.List;

public class CollectionUtils {
	public static boolean isNotEmpty(List<Object> list){
		return list != null && list.size() > 0;
	}
	public static boolean isEmpty(List<Object> list){
		return list == null || (list != null && list.size() == 0);
	}
}
