package vietnamsoft.manga;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import uk.co.senab.photoview.PhotoView;
import vietnamsoft.photo.HackyViewPager;
import vn.vietnamsoft.manga.impl.DataProcessImpl;
import vn.vietnamsoft.manga.inteface.IDataProcess;
import vn.vietnamsoft.manga.vo.Page;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

public class ViewPagerActivity extends Activity {
	protected static final String TAG = null;
	private ProgressDialog progress;
	private String admobId;
	private void showPleaseWait()
	{
		progress = ProgressDialog.show(this, "Đang tải dữ liệu",
        	    "Vui lòng đợi trong giây lát ...", true);
	}
	
	private void hidePleaseWait()
	{
		if (progress != null)
		{
			progress.dismiss();
			progress = null;
		}
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
	     getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_pager);        
        showPleaseWait();
        Intent intent = getIntent();
        String downloaded = intent.getStringExtra("downloaded");
        admobId = intent.getStringExtra("admobId");
        if (downloaded == null)
        {
	        String jsonText = "{\"url\":\"" + intent.getStringExtra("url") + "\"}";
	        new PageOnlineTask().execute("contents", jsonText);
        }
        else
        {
            SamplePagerAdapter samplePagerAdapter = new SamplePagerAdapter(null);
        	File folder = new File(downloaded);
        	File[] listOfFiles = folder.listFiles();
        	if (listOfFiles.length > 1)
        	{
	        	File[] files = new File[listOfFiles.length - 1];
	        	for (int i = 1; i < listOfFiles.length; i++)
	        	{
	        		files[i-1] = listOfFiles[i];
	        	}
	        	samplePagerAdapter.setListDownload(files);
        	}
        	ViewPager mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
    		//setContentView(mViewPager);
    		mViewPager.setAdapter(samplePagerAdapter);
    		loadAdview(admobId);
    		hidePleaseWait();
        }
       
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	private void loadAdview(String adId) {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewpage);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(65824976);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	@Override
    public void onDestroy()
    {
        getIntent().removeExtra("downloaded");
        getIntent().removeExtra("url");
        super.onDestroy();
    }
	static class SamplePagerAdapter extends PagerAdapter {
		private static String[] arr;
		private static File[] downloadedBitmaps;
		public void setListDownload(File[] listOfFile)
		{
			downloadedBitmaps = listOfFile;
		}
		public void setListUrl(String[] bmps)
		{
			arr = bmps;
		}
		public SamplePagerAdapter(String[] lst)
		{
			arr = lst;
		}
		
		@Override
		public int getCount() {
			if (arr != null)
				return arr.length;
			if (downloadedBitmaps != null)
				return downloadedBitmaps.length;
			return 0;
		}

		@Override
		public View instantiateItem(ViewGroup container, int position) {
			PhotoView photoView = new PhotoView(container.getContext());
			if (downloadedBitmaps != null)
			{
				Drawable drw = Drawable.createFromPath(downloadedBitmaps[position].getAbsolutePath());
				photoView.setImageDrawable(drw);
			}
			else
			{
				UrlImageViewHelper.setUrlDrawable(photoView, arr[position],R.drawable.loading);
			}
			// Now just add PhotoView to ViewPager and return it
			container.addView(photoView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			return photoView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

	}
	class PageOnlineTask extends AsyncTask<String, Void, List<Page>>{
    	@Override
    	protected List<Page> doInBackground(String... params) {
    		long time = System.currentTimeMillis();
    		long ex = 0;
    		List<Page> res = new ArrayList<Page>();
    		IDataProcess dt = new DataProcessImpl();
    		res = dt.getListPage(params[0], params[1]);
    		int total = 0;
    		while ((res == null || (res != null && res.size() == 0)))
    		{
    			ex = (System.currentTimeMillis() - time);
    			if (ex >= 4000)
    			{
    				time = System.currentTimeMillis();
    				total ++;
    				res = dt.getListPage(params[0], params[1]);
    			}
    			if (total > 6)
    			{
    				break;
    			}
    		}
    		if ((res == null || (res != null && res.size() == 0)) && total > 6)
    		{
    			//T.makeText(ViewPagerActivity.this, IMangaConst.SERVER_BUSY, Style.INFO);
    		}
    		return res;
    	}

    	@Override
    	protected void onPostExecute(List<Page> result) {
            SamplePagerAdapter samplePagerAdapter = new SamplePagerAdapter(null);
    		List<String> urlImages = new ArrayList<String>();
			for (Page page : result)
			{
				urlImages.add(page.getUrlImage());
			}
			samplePagerAdapter.setListUrl(urlImages.toArray(new String[urlImages.size()]));
			samplePagerAdapter.setListDownload(null);
			ViewPager mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
    		//setContentView(mViewPager);
    		mViewPager.setAdapter(samplePagerAdapter);
    		loadAdview(admobId);
    		hidePleaseWait();
    	}

    	@Override
    	protected void onPreExecute() {
    	}

    	@Override
    	protected void onProgressUpdate(Void... values) {
    	}
	}
}
