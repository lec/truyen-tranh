/*
 * MangaImpl.java
 *
 * Copyright by Orell F�ssli Wirtschaftsinformationen AG
 * Z�rich
 * All rights reserved.
 */
package vn.vietnamsoft.manga.vo;

import java.io.Serializable;


public class UpdateApp implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -4158720126503628258L;
	private String admobId;
    private String content;
    private String type;
    public String getAdmobId() {
		return admobId;
	}
	public void setAdmobId(String admobId) {
		this.admobId = admobId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
    private String url;
    

}
/*
 * Changes:
 * $Log: $
 */