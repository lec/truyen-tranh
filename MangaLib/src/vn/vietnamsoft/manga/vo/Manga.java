/*
 * Manga.java
 *
 * Copyright by Orell F�ssli Wirtschaftsinformationen AG
 * Z�rich
 * All rights reserved.
 */
package vn.vietnamsoft.manga.vo;

import java.io.Serializable;

/**
 * The Class Manga.
 *
 * @author lptchi
 * @version $Revision:  $
 */
public class Manga implements Serializable
{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -6063701626574419185L;

	/** The content. */
    private String content;
    
    /** The image. */
    private String image;
    
    /** The url. */
    private String url;
    
    /** The type. */
    private String type;
    private int id;
    /** The name. */
    private String name;
    private String namechapter; /**
     * Gets the image.
     *
     * @return the image
     */
    public String getImage()
    {
        return image;
    }

    /**
     * Sets the image.
     *
     * @param image the image
     * @return the manga
     */
    public Manga setImage(String image)
    {
        this.image = image;
        return this;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * Sets the url.
     *
     * @param url the url
     * @return the manga
     */
    public Manga setUrl(String url)
    {
        this.url = url;
        return this;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType()
    {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type the type
     * @return the manga
     */
    public Manga setType(String type)
    {
        this.type = type;
        return this;
    }

    /**
     * Sets the content.
     *
     * @param content the content
     * @return the manga
     */
    public Manga setContent(String content)
    {
        this.content = content;
        return this;
    }

    /**
     * Gets the content.
     *
     * @return the content
     */
    public String getContent()
    {
        return content;
    }

    /**
     * Sets the name.
     *
     * @param name the name
     * @return the manga
     */
    public Manga setName(String name)
    {
        this.name = name;
        return this;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

	public String getNamechapter() {
		return namechapter;
	}

	public void setNamechapter(String namechapter) {
		this.namechapter = namechapter;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
    
}


/*
 * Changes:
 * $Log: $
 */