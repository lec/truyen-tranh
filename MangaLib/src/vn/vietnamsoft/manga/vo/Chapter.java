/*
 * MangaImpl.java
 *
 * Copyright by Orell F�ssli Wirtschaftsinformationen AG
 * Z�rich
 * All rights reserved.
 */
package vn.vietnamsoft.manga.vo;

/**
 * The Class Manga.
 */
public class Chapter
{
    private String id;
    private String name;
    private String url;
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }


    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }


    
    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }


    
    /**
     * Sets the url.
     *
     * @param url the url
     * @return the chapter
     */
    public Chapter setUrl(String url)
    {
        this.url = url;
        return this;
    }


    /**
     * Sets the id.
     *
     * @param id the id
     * @return the chapter
     */
    public Chapter setId(String id)
    {
        this.id = id;
        return this;
    }


    /**
     * Sets the name.
     *
     * @param name the name
     * @return the chapter
     */
    public Chapter setName(String name)
    {
        this.name = name;
        return this;
    }

}


/*
 * Changes:
 * $Log: $
 */