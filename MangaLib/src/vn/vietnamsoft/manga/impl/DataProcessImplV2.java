package vn.vietnamsoft.manga.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import vn.vietnamsoft.manga.inteface.IDataProcess;
import vn.vietnamsoft.manga.util.MangaFieldConst;
import vn.vietnamsoft.manga.util.MangaRequestUtil;
import vn.vietnamsoft.manga.vo.Chapter;
import vn.vietnamsoft.manga.vo.Manga;
import vn.vietnamsoft.manga.vo.Page;
import vn.vietnamsoft.manga.vo.UpdateApp;

public class DataProcessImplV2 implements IDataProcess {

	@Override
	public List<Manga> getListManga(String urlType, String jsonData) {
		List<Manga> getListManga = new ArrayList<Manga>();
        String url = urlType + "/" + jsonData;
        try
        {
            JsonNode node = MangaRequestUtil.getHttpRequest(url);
            Iterator<JsonNode> ite = node.iterator();
            while(ite.hasNext())
            {
                JsonNode nd = ite.next();
                Manga manga = new Manga();
                //manga.setContent(nd.get(MangaFieldConst.CONTENT) == null ? "" : nd.get(MangaFieldConst.CONTENT).asText());
                manga.setImage(nd.get(MangaFieldConst.IMAGE) == null ? "" : nd.get(MangaFieldConst.IMAGE).asText());
                //manga.setType(nd.get(MangaFieldConst.TYPE) == null ? "" : nd.get(MangaFieldConst.TYPE).asText());
                //manga.setUrl(nd.get(MangaFieldConst.LINK) == null ? "" : nd.get(MangaFieldConst.LINK).asText());
                manga.setName(nd.get(MangaFieldConst.NAME) == null ? "" : nd.get(MangaFieldConst.NAME).asText());
                manga.setId(nd.get(MangaFieldConst.ID) == null ? 0 : nd.get(MangaFieldConst.ID).asInt());
                //manga.setNamechapter(nd.get(MangaFieldConst.CHAPTER_NAME) == null ? "" : nd.get(MangaFieldConst.CHAPTER_NAME).asText());
                getListManga.add(manga);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return getListManga;
	}

	@Override
	public List<Chapter> getListChapter(String urlType, String jsonData) {
		List<Chapter> getListChapter = new ArrayList<Chapter>();
        String url = urlType + "/" + jsonData;
        try
        {
            JsonNode node = MangaRequestUtil.getHttpRequest(url);
            Iterator<JsonNode> ite = node.iterator();
            while(ite.hasNext())
            {
                JsonNode nd = ite.next();
                Chapter chapter = new Chapter();
                chapter.setId(nd.get(MangaFieldConst.ID) == null ? "" : nd.get(MangaFieldConst.ID).asText());
                //chapter.setUrl(nd.get(MangaFieldConst.URL) == null ? "" : nd.get(MangaFieldConst.URL).asText());
                chapter.setName(nd.get(MangaFieldConst.NAME) == null ? "" : nd.get(MangaFieldConst.NAME).asText());
                getListChapter.add(chapter);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return getListChapter;
	}

	@Override
	public List<Page> getListPage(String urlType, String jsonData) {
		List<Page> getListPage = new ArrayList<Page>();
        String url = urlType + "/" + jsonData;
        try
        {
            JsonNode node = MangaRequestUtil.getHttpRequest(url);
            Iterator<JsonNode> ite = node.iterator();
            int id = 0;
            while(ite.hasNext())
            {
                JsonNode nd = ite.next();
                Page page = new Page();
                page.setId(nd.get(MangaFieldConst.ID) == null ? String.valueOf(id) : nd.get(MangaFieldConst.ID).asText());
                page.setUrlImage(nd.get(MangaFieldConst.IMG) == null ? "" : nd.get(MangaFieldConst.IMG).asText());
                getListPage.add(page);
                id ++;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return getListPage;
	}

	@Override
	public UpdateApp getUpdateApp() {
		// TODO Auto-generated method stub
		return null;
	}

}
