/*
 * DataProcess.java
 *
 * Copyright by Orell F�ssli Wirtschaftsinformationen AG
 * Z�rich
 * All rights reserved.
 */
package vn.vietnamsoft.manga.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vn.vietnamsoft.manga.inteface.IDataProcess;
import vn.vietnamsoft.manga.util.MangaFieldConst;
import vn.vietnamsoft.manga.util.MangaRequestUtil;
import vn.vietnamsoft.manga.vo.Chapter;
import vn.vietnamsoft.manga.vo.Manga;
import vn.vietnamsoft.manga.vo.Page;
import vn.vietnamsoft.manga.vo.UpdateApp;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * The Class DataProcess.
 */
public class DataProcessImpl implements IDataProcess
{

    /**
     * @see vn.vietnamsoft.manga.inteface.IDataProcess#getListManga(java.lang.String, java.lang.String)
     */
    @Override
    public List<Manga> getListManga(String urlType, String jsonData)
    {
        List<Manga> getListManga = new ArrayList<Manga>();
        String url = MangaRequestUtil.generateUrl(urlType, MangaFieldConst.SIGN, jsonData);
        System.out.println(url);
        try
        {
            JsonNode node = MangaRequestUtil.getHttpRequest(url);
            Iterator<JsonNode> ite = node.iterator();
            while(ite.hasNext())
            {
                JsonNode nd = ite.next();
                Manga manga = new Manga();
                manga.setContent(nd.get(MangaFieldConst.CONTENT) == null ? "" : nd.get(MangaFieldConst.CONTENT).asText());
                manga.setImage(nd.get(MangaFieldConst.IMAGE) == null ? "" : nd.get(MangaFieldConst.IMAGE).asText());
                manga.setType(nd.get(MangaFieldConst.TYPE) == null ? "" : nd.get(MangaFieldConst.TYPE).asText());
                manga.setUrl(nd.get(MangaFieldConst.LINK) == null ? "" : nd.get(MangaFieldConst.LINK).asText());
                manga.setName(nd.get(MangaFieldConst.NAME) == null ? "" : nd.get(MangaFieldConst.NAME).asText());
                manga.setNamechapter(nd.get(MangaFieldConst.CHAPTER_NAME) == null ? "" : nd.get(MangaFieldConst.CHAPTER_NAME).asText());
                getListManga.add(manga);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return getListManga;
    }


    /**
     * @see vn.vietnamsoft.manga.inteface.IDataProcess#getListChapter(java.lang.String, java.lang.String)
     */
    @Override
    public List<Chapter> getListChapter(String urlType, String jsonData)
    {
        List<Chapter> getListChapter = new ArrayList<Chapter>();
        String url = urlType + "/" + jsonData;
        try
        {
            JsonNode node = MangaRequestUtil.getHttpRequest(url);
            Iterator<JsonNode> ite = node.iterator();
            while(ite.hasNext())
            {
                JsonNode nd = ite.next();
                Chapter chapter = new Chapter();
                chapter.setId(nd.get(MangaFieldConst.ID) == null ? "" : nd.get(MangaFieldConst.ID).asText());
                chapter.setUrl(nd.get(MangaFieldConst.URL) == null ? "" : nd.get(MangaFieldConst.URL).asText());
                chapter.setName(nd.get(MangaFieldConst.NAME) == null ? "" : nd.get(MangaFieldConst.NAME).asText());
                getListChapter.add(chapter);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return getListChapter;
    }


    /**
     * @see vn.vietnamsoft.manga.inteface.IDataProcess#getListPage(java.lang.String, java.lang.String)
     */
    @Override
    public List<Page> getListPage(String urlType, String jsonData)
    {
        List<Page> getListPage = new ArrayList<Page>();
        String url = MangaRequestUtil.generateUrl(urlType, MangaFieldConst.SIGN, jsonData);
        try
        {
            JsonNode node = MangaRequestUtil.getHttpRequest(url);
            Iterator<JsonNode> ite = node.iterator();
            while(ite.hasNext())
            {
                JsonNode nd = ite.next();
                Page page = new Page();
                page.setId(nd.get(MangaFieldConst.ID) == null ? "" : nd.get(MangaFieldConst.ID).asText());
                page.setUrlImage(nd.get(MangaFieldConst.IMG) == null ? "" : nd.get(MangaFieldConst.IMG).asText());
                getListPage.add(page);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return getListPage;
    }


	@Override
	public UpdateApp getUpdateApp() {
		UpdateApp updateApp = null;
		String url = MangaRequestUtil.generateUrl(MangaFieldConst.CHECK_UPDATE, MangaFieldConst.SIGN);
		try
        {
            JsonNode node = MangaRequestUtil.getHttpRequest(url);
            updateApp = new UpdateApp();
            updateApp.setAdmobId(node.get(MangaFieldConst.ADMOB_ID) == null ? "" : node.get(MangaFieldConst.ADMOB_ID).asText());
            updateApp.setContent(node.get(MangaFieldConst.CONTENT) == null ? "" : node.get(MangaFieldConst.CONTENT).asText());
            updateApp.setUrl(node.get(MangaFieldConst.URL) == null ? "" : node.get(MangaFieldConst.URL).asText());
            updateApp.setType(node.get(MangaFieldConst.TYPE) == null ? "" : node.get(MangaFieldConst.TYPE).asText());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
		return updateApp;
	}
    
}


/*
 * Changes:
 * $Log: $
 */