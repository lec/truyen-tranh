/*
 * IDataProcess.java
 *
 * Copyright by Orell F�ssli Wirtschaftsinformationen AG
 * Z�rich
 * All rights reserved.
 */
package vn.vietnamsoft.manga.inteface;

import java.util.List;

import vn.vietnamsoft.manga.vo.Chapter;
import vn.vietnamsoft.manga.vo.Manga;
import vn.vietnamsoft.manga.vo.Page;
import vn.vietnamsoft.manga.vo.UpdateApp;

/**
 * The Interface IDataProcess.
 */
public interface IDataProcess
{
    
    /**
     * Gets the list manga.
     *
     * @param urlType the type
     * @param jsonData the json data
     * @return the list manga
     */
    List<Manga> getListManga(String urlType, String jsonData);
    
    /**
     * Gets the list chapter.
     *
     * @param urlType the type
     * @param jsonData the json data
     * @return the list chapter
     */
    List<Chapter> getListChapter(String urlType, String jsonData);
    
    /**
     * Gets the list page.
     *
     * @param urlType the type
     * @param jsonData the json data
     * @return the list page
     */
    List<Page> getListPage(String urlType, String jsonData);
    
    UpdateApp getUpdateApp();
}


/*
 * Changes:
 * $Log: $
 */