package vn.vietnamsoft.manga.util;


public class MyAES {
//	private static String algorithm = "AES";
//	private static String aesType = "AES/ECB/NoPadding";
//	private static byte[] keyValue =  MangaFieldConst.PRIVATEKEY.getBytes();
	static {
        System.loadLibrary("encrypt");
    }
	public static native String  stringEncrypt(String data);
	public static native String  stringDecrypt(String data);
	// Performs Encryption
	public static String encrypt(String plainText) throws Exception {
//		Key key = generateKey();
//		Cipher chiper = Cipher.getInstance(aesType);
//		chiper.init(Cipher.ENCRYPT_MODE, key);
//		byte[] encVal = chiper.doFinal(plainText.getBytes("UTF-8"));
//		Log.v("tdtanvn","tdtanvn 1 "+plainText.toLowerCase());
        String encryptedValue = stringEncrypt(plainText);
//       Log.v("tdtanvn","tdtanvn "+encryptedValue);
		return encryptedValue;
	}
	//encrypted text in base 64 decoded byte []
	public static String decrypt(String encryptedText) throws Exception {
//		// generate key
//		Key key = generateKey();
//		Cipher chiper = Cipher.getInstance(aesType);
//		chiper.init(Cipher.DECRYPT_MODE, key);
//		byte[] decValue = chiper.doFinal(encryptedText.);
//		String decryptedValue = new String(decValue);
//		return decryptedValue;
		String encryptedValue = stringDecrypt(encryptedText);
		return encryptedValue;
	}

	// generateKey() is used to generate a secret key for AES algorithm
//	private static Key generateKey() throws Exception {
//		Key key = new SecretKeySpec(keyValue, algorithm);
//		return key;
//	}

}
