/*
 * MangaRequest.java
 *
 * Copyright by Orell F�ssli Wirtschaftsinformationen AG
 * Z�rich
 * All rights reserved.
 */
package vn.vietnamsoft.manga.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class MangaRequest.
 */
public class MangaRequestUtil
{
    /**
     * Gets the http request.
     *
     * @param request the request
     * @return the http request
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static JsonNode getHttpRequest(String request) throws IOException
    {
        URL url = new URL(request);
        URLConnection yc = url.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                yc.getInputStream()));
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(in);
    }
    public static String normalizeAES(String text)
    {
    	String js = text;
    	int length = text.length();
    	int pd = 0;
    	if (length < 16){
    		pd = 16 - length;
    	}
    	else
    	{
    		pd = length % 16;
    		pd = 16 - pd;
    	}
    	if (pd != 0)
    	{
    		for (int i = 0; i < pd; i++)
    		{
    			js += "/";
    		}
    	}
    	return js;
    }
    public static String encodeJson(String jsonData)
    {
    	String js = normalizeAES(jsonData);
    	try {
			return MyAES.encrypt(js);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return jsonData;
    }
    /**
     * Generate url.
     *
     * @param type the type
     * @param sign the sign
     * @param jsonData the json data
     * @return the string
     */
    public static String generateUrl(String type,String sign, String jsonData)
    {
        //manga?signature=tdtanvn&jsondata=eyJ0eXBlIjogMSwgInBhZ2UiOiAwfQ==
        return MangaFieldConst.HOST + type + "?signature=" + sign + "&jsondata=" + encodeJson(jsonData);
        
    }
    /**
     * Generate url.
     *
     * @param type the type
     * @param sign the sign
     * @param jsonData the json data
     * @return the string
     */
    public static String generateUrl(String type,String sign)
    {
    	return MangaFieldConst.HOST + type + "?signature=" + sign;
    	
    }
}


/*
 * Changes:
 * $Log: $
 */