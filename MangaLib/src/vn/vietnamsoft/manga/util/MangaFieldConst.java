/*
 * MangaFieldConst.java
 *
 * Copyright by Orell F�ssli Wirtschaftsinformationen AG
 * Z�rich
 * All rights reserved.
 */
package vn.vietnamsoft.manga.util;

/**
 * 
 *
 * @author lptchi
 * @version $Revision:  $
 */
public class MangaFieldConst
{
    
    /** The Constant TYPE. */
    public static final String TYPE = "type";
    
    /** The Constant ID. */
    public static final String ID = "id";
    public static final String ADMOB_ID = "admobid"; 
    /** The Constant IMAGE. */
    public static final String IMAGE = "image";
    
    /** The Constant IMG. */
    public static final String IMG = "img";
    
    /** The Constant URL. */
    public static final String URL = "url";
    
    /** The Constant SIGN. */
    public static final String SIGN = "8fe8b59d62f5746510e840e875bc2a17";
    public static final String SIGN_NEW = "2ade50fd17c531cf20934b63921d0fef";
    /** The Constant HOST. */
    //public static final String HOST = "http://162.243.234.66/api/";
    public static final String HOST = "http://caphetruyentranh.appspot.com/api/";
    /** The Constant LINK. */
    public static final String LINK = "link";
    
    /** The Constant NAME. */
    public static final String NAME = "name";
    public static final String CHAPTER_NAME = "namechapter";
    
    /** The Constant CONTENT. */
    public static final String CONTENT = "content";
    
    public static final String CHECK_UPDATE = "checkupdate";
}


/*
 * Changes:
 * $Log: $
 */